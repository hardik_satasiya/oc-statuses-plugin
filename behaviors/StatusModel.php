<?php namespace PlanetaDelEste\Statuses\Behaviors;

use System\Classes\ModelBehavior;

class StatusModel extends ModelBehavior
{
    /**
     * StatusModel constructor.
     *
     * @param \October\Rain\Database\Model $model
     */
    public function __construct($model)
    {
        parent::__construct($model);
        $model->morphMany['status'] = [
            '\PlanetaDelEste\Statuses\Models\Status',
            'name'  => 'statusable',
            'order' => 'created_at desc'
        ];
    }
}