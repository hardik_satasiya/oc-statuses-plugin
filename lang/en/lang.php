<?php

return [
    'plugin'      => [
        'name'        => 'Statuses',
        'description' => 'No description provided yet...',
    ],
    'permissions' => [
        'some_permission' => 'Permission example',
    ],
    'action'      => [
        'new'           => 'New Action',
        'label'         => 'Action',
        'create_title'  => 'Create Action',
        'update_title'  => 'Edit Action',
        'preview_title' => 'Preview Action',
        'list_title'    => 'Manage Actions',
        'field'         => [
            'color' => 'Color',
            'name'  => 'Name',
            'code'  => 'Code'
        ]
    ],
    'actions'     => [
        'delete_selected_confirm' => 'Delete the selected Actions?',
        'menu_label'              => 'Actions',
        'return_to_list'          => 'Return to Actions',
        'delete_confirm'          => 'Do you really want to delete this Action?',
        'delete_selected_success' => 'Successfully deleted the selected Actions.',
        'delete_selected_empty'   => 'There are no selected Actions to delete.',
    ],
];