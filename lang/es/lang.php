<?php

return [
    'plugin'      => [
        'name'        => 'Estados',
        'description' => 'Gestiona los estados de tus modelos',
    ],
    'permissions' => [
        'some_permission' => 'planetadeleste.statuses::lang.permissions.some_permission',
    ],
    'action'      => [
        'new'           => 'Nueva acción',
        'label'         => 'Acción',
        'create_title'  => 'Crear acción',
        'update_title'  => 'Editar acción',
        'preview_title' => 'Vista previa de la acción',
        'list_title'    => 'Administrar acciones',
        'field'         => [
            'color' => 'Color',
            'name'  => 'Nombre',
            'code'  => 'Código'
        ]
    ],
    'actions'     => [
        'delete_selected_confirm' => 'Borrar las acciones seleccionadas?',
        'menu_label'              => 'Acciones',
        'return_to_list'          => 'Volver a las acciones',
        'delete_confirm'          => 'Realmente deseas borrar esta acción?',
        'delete_selected_success' => 'La acción seleccionada fue borrada.',
        'delete_selected_empty'   => 'No has seleccionado ninguna acción para borrar.',
    ],
    'status'      => [
        'new'           => 'Nuevo estado',
        'label'         => 'Estado',
        'create_title'  => 'Crear estado',
        'update_title'  => 'Editar estado',
        'preview_title' => 'Vista previa del estado',
        'list_title'    => 'Administrar estados',
        'field'         => [
            'info' => 'Descripción',
        ]
    ],
];