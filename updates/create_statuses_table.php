<?php namespace PlanetaDelEste\Statuses\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateStatusesTable extends Migration
{
    public function up()
    {
        $this->down();
        Schema::create('planetadeleste_statuses_statuses', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('action_id', false, true)->index();
            $table->morphs('statusable', 'statuses');
            $table->text('info')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('planetadeleste_statuses_statuses');
    }
}
