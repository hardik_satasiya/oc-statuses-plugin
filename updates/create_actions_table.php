<?php namespace PlanetaDelEste\Statuses\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateActionsTable extends Migration
{
    public function up()
    {
        Schema::create('planetadeleste_statuses_actions', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 100);
            $table->string('code', 50);
            $table->string('color', 50)->default('#cccccc')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('planetadeleste_statuses_actions');
    }
}
