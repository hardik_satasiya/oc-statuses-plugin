<?php namespace PlanetaDelEste\Statuses\Models;

use Model;

/**
 * Status Model
 * @property string|null                            info
 * @property \PlanetaDelEste\Statuses\Models\Action action
 *
 * @method \October\Rain\Database\Relations\BelongsTo action()
 */
class Status extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'planetadeleste_statuses_statuses';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['info'];

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'action' => ['\PlanetaDelEste\Statuses\Models\Action']
    ];
    public $morphTo = ['statusable' => []];

    public function getNameAttribute()
    {
        return ($this->action) ? $this->action->name : null;
    }

    public function getCodeAttribute()
    {
        return ($this->action) ? $this->action->code : null;
    }

    public function getColorAttribute()
    {
        return ($this->action) ? $this->action->color : null;
    }
}
