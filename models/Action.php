<?php namespace PlanetaDelEste\Statuses\Models;

use Model;
use October\Rain\Database\Traits\Sluggable;
use October\Rain\Database\Traits\Validation;

/**
 * Action Model
 *
 * @property string name
 * @property string code
 * @property string color
 *
 * @method \October\Rain\Database\Relations\HasMany statuses()
 * @method static \Illuminate\Database\Query\Builder code(string $code)
 */
class Action extends Model
{
    use Validation;
    use Sluggable;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'planetadeleste_statuses_actions';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['name', 'code', 'color'];

    /**
     * @var array Relations
     */
    public $hasMany = [
        'statuses' => ['\PlanetaDelEste\Statuses\Models\Status']
    ];

    public $rules = [
        'name' => 'required',
        'code' => 'required'
    ];

    /**
     * @var array Generate slugs for these attributes.
     */
    protected $slugs = ['code' => 'name'];

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['name'];

    public function scopeCode($query, $code)
    {
        return $query->where('code', '=', $code);
    }
}
