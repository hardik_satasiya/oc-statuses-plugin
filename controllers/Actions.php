<?php namespace PlanetaDelEste\Statuses\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Lang;
use PlanetaDelEste\Statuses\Models\Action;

/**
 * Actions Back-end Controller
 */
class Actions extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('PlanetaDelEste.Statuses', 'statuses', 'actions');
    }

    /**
     * Deleted checked actions.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $actionId) {
                if (!$action = Action::find($actionId)) continue;
                $action->delete();
            }

            Flash::success(Lang::get('planetadeleste.statuses::lang.actions.delete_selected_success'));
        }
        else {
            Flash::error(Lang::get('planetadeleste.statuses::lang.actions.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}
